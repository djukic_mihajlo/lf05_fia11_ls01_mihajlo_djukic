import java.util.Scanner;

public class Treppe {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        System.out.println("Geben Sie die Hoehe der Treppe ein");
        int height = inputHeight();
        boolean checker = checker(height);
       
        if (checker == true) {
           
            int possibilities = possibilities(height);
            System.out.println("Anzahl der Moeglichkeiten: " + possibilities);
           
        } else {
           
            System.err.println("Fehler, die Hoehe der Treppe muss eine natuerliche Zahl groesser 0 sein!");
           
        } // end of if command check if height is ok input

    } // end of method main

    /**
     * input an int (steps of a staircase), save it in int height
     *
     * @return int height, steps of a staircase
     */
    private static int inputHeight() {

        Scanner scanner = new Scanner(System.in);
        int height = scanner.nextInt();

        scanner.close();

        return height;

    } // end of method inputHeight

    /**
     * checks if int height is greater 0 or not
     *
     * @param int height, steps of a staircase
     * @return true, if int height > 0 | false, if int height is not > 0
     */
    private static boolean checker(int height) {

        if (height > 0) {

            return true;

        } else {

            return false;

        } // end of if command check if height is ok

    } // end of method checker

    /**
     * calculates the possibilities of completing the staircase
     *
     * recursive call of possibilities with (int height - 1) +
     * recursive call of possibilities with (int height - 2)
     * until height is 1 -> return 1
     * or height is 2 -> return 2
     *
     * @param int height, steps of a staircase (left)
     * @return int, possibilities of completing the staircase
     */
    private static int possibilities(int height) {
       
        if(height == 1) {
           
            return 1;
           
        }
       
        if(height == 2) {
           
            return 2;
           
        }
       
        return possibilities(height - 1) + possibilities(height - 2);       
       
    } // end of method possibilities

} // end of class treppen