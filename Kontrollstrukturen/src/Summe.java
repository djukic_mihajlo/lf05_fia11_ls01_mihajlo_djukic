
public class Summe {

	public static void main(String[] args) {

		// Z�hlschleife (for)
		int value = 0;
		for (int i = 1; i <= 100; i++) {
			value = value + i;
			System.out.println(value);
		}

		// vorpr�fende Schleife (while)
		int result = 0;
		int counter = 1;
		while (counter <= 100) {
			result += counter;
			System.out.println(result);
			counter++;
		}

		// nachpr�fende Schleife (do while)
		int begin = 0;
		int zaehler = 1;
		do {
			begin += zaehler;
			System.out.println(begin);
			zaehler++;
		} while (zaehler <= 100);

	}

}
