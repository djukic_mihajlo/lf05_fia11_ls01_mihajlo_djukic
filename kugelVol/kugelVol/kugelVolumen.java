package kugelVol;

import java.util.Scanner;

public class kugelVolumen {

	public static void main(String[] args) 
	{
		Scanner tastatur = new Scanner(System.in);
		double rad = 0.0;
		double vol = 0.0;
		
		rad = eingabe(tastatur);
		vol = berechneVol(rad);
		
		ausgabe(rad, vol);
	}
	
	public static double eingabe(Scanner tastatur)
	{
		System.out.print("Geben Sie den Radius ein: ");
		double rad = tastatur.nextDouble();
		
		return rad;
	}
	
	public static double berechneVol(double rad)
	{
		double volume = (4.0/3.0) * Math.PI * (rad * rad * rad);
		return volume;
	}
	
	public static void ausgabe(double rad, double vol)
	{
		System.out.println("Bei einem Radius von " + rad +" hat die Kugel einen Volumen von : " + vol);
	}
	

}
