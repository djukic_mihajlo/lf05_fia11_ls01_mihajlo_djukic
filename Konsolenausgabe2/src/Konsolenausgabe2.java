
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
		//Aufgabe 1
		System.out.println("Aufgabe 1\n");
		
		String s = "*";
		System.out.printf("%10s", s);  //oben
		System.out.printf("%s", s);  //oben
		System.out.printf("\n%7s", s);  //mitte oben
		System.out.printf("%7s", s);  //mitte oben
		System.out.printf("\n%7s", s);  //mitte unten
		System.out.printf("%7s", s);  //mitte unten
		System.out.printf("\n%10s", s);  //unten
		System.out.printf("%s", s);  //unten
		
		
		
		
		//Aufgabe 2
		System.out.println("\n\nAufgabe 2");
		
		
		
		//linksbŁndig
		System.out.printf("\n%-5s =", "0!");
		System.out.printf(" %-19s", " ");
		System.out.printf(" %4s", "=   0");
		
		System.out.printf("\n%-5s =", "1!");
		System.out.printf(" %-19s", "1");
		System.out.printf(" %4s", "=   1");
		
		System.out.printf("\n%-5s =", "2!");
		System.out.printf(" %-19s", "1 * 2");
		System.out.printf(" %4s", "=   2");
		
		
		System.out.printf("\n%-5s =", "3!");
		System.out.printf(" %-19s", "1 * 2 * 3");
		System.out.printf(" %4s", "=   6");
		
		System.out.printf("\n%-5s =", "4!");
		System.out.printf(" %-19s", "1 * 2 * 3 * 4");
		System.out.printf(" %4s", "=  24");
		
		System.out.printf("\n%-5s =", "5!");
		System.out.printf(" %-19s", "1 * 2 * 3 * 4 * 5");
		System.out.printf(" %4s", "= 120");
		
		
		
		
		
		
		//Aufgabe 3
		System.out.println("\n\nAufgabe 3\n");
		
		
		String C = "Celsius";
		String F = "Fahrenheit";
		String T = "----------------------";
		
		
		
		int c1 = -20;
		int c2 = -10;
		int c3 =   0;
		int c4 =  20;
		int c5 =  30;
		
		
		
		float f1 = -28.8889f;
		float f2 = -23.3333f;
		float f3 = - 7.7778f;
		float f4 = - 6.6667f;
		float f5 = - 1.1111f;
		
		
		System.out.printf("%-10s|", C);
		System.out.printf("%10s\n", F);
		
		System.out.printf("%22s\n", T);
		
		System.out.printf("%+-10d|", c1);
		System.out.printf("%-10.2f\n", f1);
		
		System.out.printf("%+-10d|", c2);
		System.out.printf("%-10.2f\n", f2);
		
		System.out.printf("%+-10d|", c3);
		System.out.printf("%-10.2f\n", f3);
		
		System.out.printf("%+-10d|", c4);
		System.out.printf("%-10.2f\n", f4);
		
		System.out.printf("%+-10d|", c5);
		System.out.printf("%-10.2f\n", f5);
		
		
		
		
		
		

	}

}
