import java.util.Scanner;

class LOESER_A3_punkt_4 {

	public static double fahrkartenbestellungErfassen() {
		int anzahlTickets;
		double ticketPreis;
		Scanner tastatur = new Scanner(System.in);

		System.out.print("Ticketpreis (EURO-Cent): ");
		ticketPreis = tastatur.nextDouble();

		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();

		return ticketPreis * anzahlTickets;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlen: %4.2f � %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(255);
		}
		System.out.println("\n\n");
	}

	public static void warte(int milisekunde) {
		try {
			Thread.sleep(milisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der R�ckgabebetrag in H�he von %4.2f Euro %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag = runden(rueckgabebetrag -= 2.0);
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag = runden(rueckgabebetrag -= 1.0);
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.5);
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.2);
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Müzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.1);
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.05);
			}
		}
	}

	public static double runden(double zahl) {
		zahl = Math.round(zahl * 100)/100.00;
		//System.out.println("Extra: " + zahl);
		return zahl;
	
    }

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println("" + betrag + " " + einheit);
	}

	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;

		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rueckgabebetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}
}

