import java.util.Scanner;

class Fahrkartenautomat_mit_Methoden {
	public static void main(String[] args) {
		float zuZahlenderBetrag = fahrkartenbestellungErfassen();
		float r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(zuZahlenderBetrag, r�ckgabebetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	// ==============================================METHODEN==============================================================

	public static float fahrkartenbestellungErfassen() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n");		
		System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n");				
		System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)\n");		
		System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\");
		int ticketWahl = myScanner.nextInt();
		if (ticketWahl == 1) {
			System.out.println("Ihre Wahl: " + ticketWahl);
		}
		if (ticketWahl == 2) {
			System.out.println("Ihre Wahl: " + ticketWahl);
		}
		if (ticketWahl == 3) {
			System.out.println("Ihre Wahl: " + ticketWahl);
		}
		else {
			System.out.println("Ihre Wahl: " + ticketWahl + "\n>>falsche Eingabe<<\n");
		}

		System.out.print("Anzahl der Tickets: ");
		byte anzahl = myScanner.nextByte();
		if (anzahl > 10) {
			System.out.println("Zu viele Tickets augew�hlt!\nSetze die Ticketzahl auf 1!");
			anzahl = 1;
		}
		if (anzahl <= 0) {
			System.out.println("Sie haben 0 oder negative Tickets augew�hlt!\nSetze die Ticketzahl auf 1!");
			anzahl = 1;
		}

		return ticketWahl * anzahl;
	}

	public static float fahrkartenBezahlen(float zuZahlenderBetrag) {
		// Geldeinwurf
		// -----------
		float eingezahlterGesamtbetrag;
		float eingeworfeneM�nze;
		Scanner myScanner = new Scanner(System.in);

		eingezahlterGesamtbetrag = 0.0f;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f \n ", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = myScanner.nextFloat();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(float zuZahlenderBetrag, float eingezahlterGesamtbetrag) {
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		float r�ckgabebetrag;
		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}

		}

	}
}
