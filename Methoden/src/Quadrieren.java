import java.util.Scanner;


public class Quadrieren {
   
	public static void main(String[] args) {
		titel();
		
		
		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		Scanner tastatur = new Scanner(System.in);
		double x = eingabe(tastatur);
		
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis = verarbeitung(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		ausgabe(x, ergebnis);
	}	
	
	
	
	
	public static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x� ");
		System.out.println("------------------------------------------------------");
	}
	
	
	//Methoden nach EVA-Prinzip
	public static double eingabe(Scanner tastatur) {
		System.out.print("Geben Sie einen x-Wert an: ");
		double x = tastatur.nextDouble();
		return x;
	}
	
	public static double verarbeitung(double x) {
		double ergebnis= x * x;
		return ergebnis;
	}
	
	public static void ausgabe(double x, double xquadrat) {
		System.out.printf("\nx = %.2f und x�= %.2f\n", x, xquadrat);		
	}
	
}
