import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
	   titel();
      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
	   Scanner tastatur = new Scanner(System.in);
	   double x = eingabe(tastatur);
	   double y = eingabe(tastatur);
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      double ergebnis = verarbeitung(x, y);
      
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      ausgabe(x,y,ergebnis);
   }
   
   public static void titel() {
		System.out.println("Dieses Programm berechnet den Mittelwert");
		System.out.println("------------------------------------------------------");
	}   
   
   
   public static void ausgabe(double x, double y, double mittelwert) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, mittelwert);
   }
   
   public static double verarbeitung(double x, double y) {
	   double ergebnis = (x + y) / 2.0;
	   return ergebnis;
   }
   
   
   public static double eingabe(Scanner tastatur) {
	   System.out.print("Geben Sie eine Zahl an: ");
	   double z = tastatur.nextDouble();
	   return z;
   }
  
}
